package crypto.soft.cryptongy.feature.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import crypto.soft.cryptongy.R;
import crypto.soft.cryptongy.feature.aboutUs.AboutUsActivity;
import crypto.soft.cryptongy.feature.account.AccountActivity;
import crypto.soft.cryptongy.feature.setting.SettingActivity;


public class MainActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void openSetting(View view){
        startActivity(new Intent(this, SettingActivity.class));
    }

    public void openAboutUs(View view){
        startActivity(new Intent(this, AboutUsActivity.class));
    }

    public void openAccount(View view){
        startActivity(new Intent(this, AccountActivity.class));
    }
}
